<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Foundation\Auth\User as Authenticatable; 
 
class Pengguna extends Authenticatable 
{  
	protected $table = 'pengguna';
	public function Pembeli()
	{   
		return $this->hasMany(Pembeli::class);  
	}

	public function Admin()
	{   
		return $this->hasMany(Admin::class);  
	} 
}