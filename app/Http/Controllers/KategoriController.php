<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Kategori;

class KategoriController extends Controller
{
    public function awal(){
    	$kategori = Kategori::all();
   		return view('buku.kategori',compact('kategori'));
   }
   public function tambah(){
         return view('buku.tambahKategori');
   }   
   public function simpan(Request $input){
      $this->validate($input,[
        'deskripsi' => 'required',        
   ]);

   		$kategori = new Kategori();
   		$kategori->deskripsi = $input->deskripsi;
   		$kategori->save();
   		return redirect('kategori');
   }
   public function edit($id){
   	$kategori = Kategori::find($id);
   	return view('buku.editkategori')->with(array('kategori'=>$kategori));
   }
  /* public function update($id, Request $input){
   		$kategori = Kategori::find($id);
   		return view('buku.editkategori')->with(array('kategori'=>$kategori));
   }*/
   public function update($id, Request $input){
   		$kategori = Kategori::find($id);
   		$kategori->deskripsi = $input->deskripsi;
   		$kategori->save();
   		return redirect('kategori');
   }
   public function hapus($id){
   		$kategori = Kategori::find($id);
   		$kategori->delete();
   		return redirect('kategori');
   }
   public function tampil()
   {
      $buku = Kategori::find(3)->buku;
      //$pembeli = Pengguna::all(); 
       return $buku;  
   }
}
