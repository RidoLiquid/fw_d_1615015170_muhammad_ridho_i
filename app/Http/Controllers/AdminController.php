<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Pengguna;
use App\Admin;

class Admincontroller extends Controller
{
   public function awal()
   {
   		$admin = Admin::all();
   		return view('admin.app',compact('admin'));
   }
   public function tambah(){
   		return view('admin.tambah');
   }
   public function simpan(Request $input){
         $this->validate($input,[
        'nama' => 'required',
        'notlp' => 'alpha_num',
        'email' => 'required',
        'alamat' => 'required',        
   ]);

         $pengguna = new Pengguna();
        $pengguna->username = $input->username;
        $pengguna->password = $input->password;
        //$pengguna->level = "village";
        $pengguna->save();

   		$admin = new Admin();
   		$admin->nama = $input->nama;
   		$admin->notlp = $input->notlp;
   		$admin->email = $input->email;
   		$admin->alamat = $input->alamat;
         $admin->pengguna_id = $pengguna->id;
   		$admin->save();
   		return redirect('admin');


   }
   public function edit($id){
   	$admin = Admin::find($id);
   	return view('admin.edit')->with(array('admin'=>$admin));
   }
   public function update($id, Request $input){
   		$admin = Admin::find($id);
   		$admin->nama = $input->nama;
   		$admin->notlp = $input->notlp;
   		$admin->email = $input->email;
   		$admin->alamat = $input->alamat;
   		$status = $admin->save();         
   		return redirect('admin')->with(['status'=>$status]);
         $pengguna = Pengguna::find($admin->pengguna_id);
        $pengguna->username = $input->username;
        $pengguna->password = $input->password;
        //$pengguna->level = "village";
        $pengguna->save();
   }
   public function hapus($id){
   		$admin = Admin::find($id);
   		$admin->delete();
   		return redirect('admin');
   }
}
