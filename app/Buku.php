<?php
namespace App;
 use Illuminate\Database\Eloquent\Model;

 
class Buku extends Model 
{
    protected $table = "buku";
   	public function kategori()
   	{
		return $this->belongsTo('App\Kategori');
	}

  	public function Penulis()
  	{
  	   return $this->belongsToMany(Penulis::class)
        ->withPivot('id')
        ->withTimesTamps();
  	}

  	public function Pembeli()
  	{
  	   return $this->belongsToMany(Pembeli::class);
  	}  	
} 