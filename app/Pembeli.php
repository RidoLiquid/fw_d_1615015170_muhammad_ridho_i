<?php

namespace App;
use Illuminate\Database\Eloquent\Model; 
 
class Pembeli extends Model 
{
     protected $table = 'pembeli';
         public function Pengguna()
         {
            return $this->belongsTo(Pengguna::class);
         }
         public function Buku()
  	{
  	   return $this->belongsToMany(Buku::class);
  	}    

  	public function getUsernameAttribute(){
  		return $this->pengguna->username;
	}

	public function getPasswordAttribute(){
 		return $this->pengguna->password;
	} 
}