<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeBukuPembeliTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buku_pembeli', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pembeli_id',false,true);
            $table->integer('buku_id',false,true);            
            $table->timestamps();
            $table->foreign('pembeli_id')->references('id')->on('pembeli')->onDelete('cascade');
            $table->foreign('buku_id')->references('id')->on('buku')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buku_pembeli');
    }
}
