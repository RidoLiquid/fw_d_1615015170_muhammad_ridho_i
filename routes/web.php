<?php


Route::get('/rido', function () {
    return "rido ganteng";
});

Route::get('/rido2', function () {
    return view('welcome');
});



Route::get('/coba2', 'UjiQuery@DoesntHave');
Route::get('/coba','UjiQuery@index');


Route::get('/tampil_pembeli','PenggunaController@tampil_pembeli');
Route::get('/tampil_admin','PenggunaController@tampil_admin');
Route::get('/tampil_buku','KategoriController@tampil');
Route::get('/tampil_penulis','BukuController@tampil_penulis');


Route::get('pengguna','PenggunaController@awal');
Route::get('pengguna/tambah','PenggunaController@tambah');


Route::get('/','PagesController@homepage');

Route::get('/buku',"BukuController@awal");
Route::get('/pembeli',"PembeliController@awal");
//Route::post('/pembeli/hapus/{pembeli}',"PembeliController@hapus");

Route::get('/tambah/pembeli',"PembeliController@tambah");
Route::get('/tambah',"BukuController@lihat");


Route::get('/tambah/kategori',"KategoriController@tambah");
Route::get('/tambah/penulis',"PenulisController@tambah");
Route::post('/penulis/simpan',"PenulisController@simpan");

Route::get('/penulis',"PenulisController@awal");
Route::get('/penulis/edit/{penulis}',"PenulisController@edit");
Route::post('/penulis/update/{penulis}',"PenulisController@update");
Route::get('/penulis/hapus/{penulis}',"PenulisController@hapus");

Route::get('/tambah/buku',"BukuController@tambah");
Route::post('/buku/simpan',"BukuController@simpan");
Route::get('/buku/hapus/{buku}',"BukuController@hapus");
Route::get('/buku/edit/{buku}',"BukuController@edit");
Route::post('/buku/update/{buku}',"BukuController@update");

Route::get('/','AuthController@index');
Route::get('/login','AuthController@formLogin');
Route::post('/login','AuthController@proses');
Route::get('/logout','AuthController@logout');


Route::get('/kategori',"KategoriController@awal");
Route::post('/kategori/simpan',"KategoriController@simpan");
Route::get('/kategori/edit/{kategori}',"KategoriController@edit");
Route::post('/kategori/update/{kategori}',"KategoriController@update");
Route::get('/kategori/hapus/{kategori}',"KategoriController@hapus");

Route::get('/pembeli/tambah',"PembeliController@tambah");
Route::post('/pembeli/simpan',"PembeliController@simpan");
Route::get('/pembeli',"PembeliController@awal");
Route::get('/pembeli/edit/{pembeli}',"PembeliController@edit");
Route::post('/pembeli/update/{pembeli}',"PembeliController@update");
Route::get('/pembeli/hapus/{pembeli}',"PembeliController@hapus");

Route::get('/admin',"AdminController@awal");
Route::get('admin/tambah',"AdminController@tambah");
Route::post('/admin/simpan',"AdminController@simpan");
Route::get('/admin/lihat',"AdminController@lihat");
Route::get('admin/edit/{admin}',"AdminController@edit");
Route::post('/admin/update/{admin}',"AdminController@update");
Route::get('/admin/hapus/{admin}',"AdminController@hapus");


?>