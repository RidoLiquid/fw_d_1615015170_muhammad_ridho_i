@extends('master')
@section('content')
{{ $status or ' ' }}
<div class="panel panel-info">
<div class="panel-heading">
Data Pembeli
<div class="pull-right">
Tambah Data <a href="{{ url('tambah/pembeli')}}"><img src="{{ asset('add.ico') }}" height="20"></img></a>
</div>
</div>
<div class="panel-body">
<table class="table">
<tr>
<td>Nama</td>
<td>No Telepon</td>
<td>Email</td>
<td>Alamat</td>
</tr>
@foreach($pembeli as $Pembeli)
<tr>
<td >{{ $Pembeli->nama }}</td>
<td >{{ $Pembeli->notlp}}</td>
<td >{{ $Pembeli->email }}</td>
<td >{{ $Pembeli->alamat}}</td>
<td >
<a href="{{url('pembeli/edit/'.$Pembeli->id)}}"><img src="{{ asset('edit.png') }}" height="20"></img></a>
<a href="{{url('pembeli/hapus/'.$Pembeli->id)}}"><img src="{{ asset('delete.png') }}" height="20"></img></a>
</td>
</tr>
@endforeach
</table>
</div>
</div>
@endsection