@extends('master') 
@section('content') 
{{ $status or ' ' }} 
<div class="panel panel-info">  
	<div class="panel-heading">
	   Data Admin   
	   <div class="pull-right">
	       Tambah Data <a href="{{ url('admin/tambah')}}"><img src="{{ asset('add.ico') }}" height="20"></img></a>
	          </div>
	            </div>
	              <div class="panel-body">
	                 <table class="table">
	                      <tr>
	                            <td>Nama</td> 
	                            <td>No Telepon</td>
	                            <td>Email</td>
	                            <td>Alamat</td>
	                        </tr>
	                        @foreach($admin as $Admin)
	                        <tr>
	                            <td >{{ $Admin->nama }}</td>      
	                            <td >{{ $Admin->notlp}}</td>      
	                            <td >{{ $Admin->email }}</td>      
	                            <td >{{ $Admin->alamat}}</td>      
	                            <td >            
								
								<a href="{{url('admin/edit/'.$Admin->id)}}"><img src="{{ asset('edit.png') }}" height="20"></img></a>      
								<a href="{{url('admin/hapus/'.$Admin->id)}}"><img src="{{ asset('delete.png') }}" height="20"></img></a>      
								</td>     
							</tr>     
							@endforeach    
						</table>  
					</div> 
				</div> 
				@endsection 